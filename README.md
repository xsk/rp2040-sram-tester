# rp2040-sram-tester

Micropython for [RP2040 16 Mb, RGB + User Button](https://www.aliexpress.com/item/1005006055009344.html), Testing 32Kib of Sram using an alternate checkers pattern ( 10101010, 01010101 ).

## Description

I needed to check some SRAM ICs I had around for a ZX Spectrum Project and at the same time wanted to see if it can happen with Micropython, this is the result. 

## Usage

- Connect the SRAM IC to the Pins mentioned in code and provide 5V and Ground from the Pico board to it
- Chip enable on SRAM is always set, since it is the only IC we have
- Power up the Pico, wait for it's small LED to blink, ready to test
- Press the User button, RGB will turn blue to show that it is currently testing
- If Green, SRAM is ok and the Pico board will go back to waiting for the user to press button ( led blinking ) 
- If Red, IC failed the checkers test

Takes about 30 seconds to run for 32Kib

## Roadmap

- Check timings and add screenshots from Oscilloscope 
- Replace sequential Address and IO pins with `machine.mem` based instructions
