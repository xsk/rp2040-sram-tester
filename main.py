# TODO : demystify machine.mem16 for rp2040,for Address and IO pins, good enough code follows
from machine import Pin
from time import sleep
import neopixel

# Set up
# NeoPixel
neoRing = neopixel.NeoPixel(Pin(23), 1)
START_TEST = False

# on board Button
Button = Pin(24, Pin.IN, Pin.PULL_UP)

# on board LED
led = Pin(25, Pin.OUT)

# Address lines
PinA0 = Pin(10, Pin.OUT)
PinA1 = Pin(11, Pin.OUT)
PinA2 = Pin(12, Pin.OUT)
PinA3 = Pin(13, Pin.OUT)
PinA4 = Pin(14, Pin.OUT)
PinA5 = Pin(15, Pin.OUT)
PinA6 = Pin(16, Pin.OUT)
PinA7 = Pin(17, Pin.OUT)
PinA8 = Pin(20, Pin.OUT)
PinA9 = Pin(21, Pin.OUT)
PinA10 = Pin(22, Pin.OUT)
PinA11 = Pin(26, Pin.OUT)
PinA12 = Pin(18, Pin.OUT)
PinA13 = Pin(27, Pin.OUT)
PinA14 = Pin(19, Pin.OUT)

# Output Enable
PinOE = Pin(8, Pin.OUT)
# Write Enable
PinWE = Pin(9, Pin.OUT)

# Setting initial values

# Clear RGB LED
neoRing.fill((0, 0, 0))
neoRing.write()

PinA0.value(0)
PinA1.value(0)
PinA2.value(0)
PinA3.value(0)
PinA4.value(0)
PinA5.value(0)
PinA6.value(0)
PinA7.value(0)
PinA8.value(0)
PinA9.value(0)
PinA10.value(0)
PinA11.value(0)
PinA12.value(0)
PinA13.value(0)
PinA14.value(0)

PinWE.value(1)
PinOE.value(1)


def set_address(Address):
    PinA0.value(Address & 1)
    PinA1.value((Address >> 1) & 1)
    PinA2.value((Address >> 2) & 1)
    PinA3.value((Address >> 3) & 1)
    PinA4.value((Address >> 4) & 1)
    PinA5.value((Address >> 5) & 1)
    PinA6.value((Address >> 6) & 1)
    PinA7.value((Address >> 7) & 1)
    PinA8.value((Address >> 8) & 1)
    PinA9.value((Address >> 9) & 1)
    PinA10.value((Address >> 10) & 1)
    PinA11.value((Address >> 11) & 1)
    PinA12.value((Address >> 12) & 1)
    PinA13.value((Address >> 13) & 1)
    PinA14.value((Address >> 14) & 1)


def read_values_and_test(Inverse):
    healthy = True
    PinIO1 = Pin(0, Pin.IN)
    PinIO2 = Pin(1, Pin.IN)
    PinIO3 = Pin(2, Pin.IN)
    PinIO4 = Pin(3, Pin.IN)
    PinIO5 = Pin(4, Pin.IN)
    PinIO6 = Pin(5, Pin.IN)
    PinIO7 = Pin(6, Pin.IN)
    PinIO8 = Pin(7, Pin.IN)

    PinOE.value(0)
    sleep(0.00000007)  # 70 ns

    DataValue = (
        PinIO8.value() << 7
        | PinIO7.value() << 6
        | PinIO6.value() << 5
        | PinIO5.value() << 4
        | PinIO4.value() << 3
        | PinIO3.value() << 2
        | PinIO2.value() << 1
        | PinIO1.value()
    )

    if Inverse is True:
        if DataValue != 170:  # 10101010
            healthy = False
    else:
        if DataValue != 85:  # 01010101
            healthy = False

    PinOE.value(1)

    if healthy is not True:
        print("Test failed")
        # Display Red
        neoRing.fill((int(25), 0, 0))
        neoRing.write()
        sleep(99999)  # Block "forever"


def set_pattern(Inverse):
    PinIO1 = Pin(0, Pin.OUT)
    PinIO2 = Pin(1, Pin.OUT)
    PinIO3 = Pin(2, Pin.OUT)
    PinIO4 = Pin(3, Pin.OUT)
    PinIO5 = Pin(4, Pin.OUT)
    PinIO6 = Pin(5, Pin.OUT)
    PinIO7 = Pin(6, Pin.OUT)
    PinIO8 = Pin(7, Pin.OUT)

    PinWE.value(0)
    sleep(0.00000005)  # 50 ns

    if Inverse is True:
        PinIO1.value(1)
        PinIO2.value(0)
        PinIO3.value(1)
        PinIO4.value(0)
        PinIO5.value(1)
        PinIO6.value(0)
        PinIO7.value(1)
        PinIO8.value(0)
    else:
        PinIO1.value(0)
        PinIO2.value(1)
        PinIO3.value(0)
        PinIO4.value(1)
        PinIO5.value(0)
        PinIO6.value(1)
        PinIO7.value(0)
        PinIO8.value(1)

    sleep(0.00000003)  # 30 ns
    PinWE.value(1)


def test_address(Address):
    # print("Testing Address: 0x", '{:14b}'.format(Address))
    set_address(Address)
    set_pattern(False)
    read_values_and_test(False)
    set_pattern(True)
    read_values_and_test(True)


def start_test_next(Button):
    global START_TEST
    START_TEST = True


Button.irq(trigger=Pin.IRQ_FALLING, handler=start_test_next)

while True:
    sleep(0.5)
    if START_TEST is not False:
        led.value(0)
        # Display blue
        neoRing.fill((0, 0, int(25)))
        neoRing.write()

        for i in range(32768):  # 32Kib
            test_address(i)
            # sleep(0.00001)

        # Display Green
        neoRing.fill((0, int(25), 0))
        neoRing.write()

        print("Test OK")
        START_TEST = False
    else:
        led.toggle()
